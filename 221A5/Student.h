/*
	Student.h
	Matt Gaikema
	matt.gaikema
	matt.gaikema@tamu.edu
	CSCE 221: 504

	The declarations for the Student class.
*/

#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>
#include <string>
#include <sstream>
#include <queue>
#include <regex>

namespace matt{
	
class Student {
public:	
	std::string name;
	std::string email;
	long uin;
	int score;
		
	Student() :name(""), email(""), uin(0), score(0) {};
	Student(std::string nm, std::string em, long u, int sc);

	bool is_null();
}; // End Student class

std::istream& operator>>(std::istream& is, Student& student);
std::ostream& operator<<(std::ostream& os, Student& student);

} // End matt namespace

#endif //STUDENT_H
