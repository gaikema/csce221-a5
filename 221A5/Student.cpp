/*
	Student.cpp
	Matt Gaikema
	matt.gaikema
	matt.gaikema@tamu.edu
	CSCE 221: 504

	The definitions for the Student class.
*/

#include "Student.h"

namespace matt{

Student::Student(std::string nm, std::string em, long u, int sc) :
name(nm),
email(em),
uin(u),
score(sc)
{

}

/**
	If the student has a UIN of 0, 
	then he is not a real student.
*/
bool Student::is_null(){
	if (uin == 0)
		return true;
	else
		return false;
}

/*
	A Student object is expected to be on a line alone,
	with commas separating each field.
	*/
std::istream& operator>>(std::istream& is, Student& student) {
	try{
		std::string str, item;
		std::queue<std::string> q;
		std::regex pattern("^(\\w+\\s\\w+),((\\w|@|\\.)+),(\\d{9}),(\\d{1,3})\n");

		//std::getline(is, str);
		//std::stringstream ss(str);

		// Parse `is` into student
		while (std::getline(is, item, ',')) {
			q.push(item);
		}

		if (!q.empty()) {
			// Get name
			student.name = q.front();
			q.pop();

			// Get email
			student.email = q.front();
			q.pop();

			// Get UIN
			long u = std::stol(q.front());
			student.uin = u;
			q.pop();

			// Check if score exists
			if (!q.empty()) {
				if (q.front() != " "){
					str = q.front();
					int sc = std::stoi(str);
					student.score = sc;
				}
				q.pop();
			}
		}
	}
	catch (std::invalid_argument& e){
		// do nothing lol
	}
	catch (std::regex_error& e){
		// do nothing lol
	}
	return is;
}

std::ostream& operator<<(std::ostream& os, Student& student) {
	os << student.name << "," << student.email << "," << student.uin << "," << student.score;
	return os;
}

} // End matt namespace
