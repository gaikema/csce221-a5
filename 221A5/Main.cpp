/*
	Main.cpp
	Matt Gaikema
	matt.gaikema
	matt.gaikema@tamu.edu
	CSCE 221: 504

	This is the main part of the program,
	where the data is read in and parsed.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <queue>
#include <stdlib.h>
#include "Student.h"

using namespace std;

// Define hash function
int hasher(long x, int m){
	return x % m;
}

int main(){
	try{
		const string pathname = "221-16a-A5-suppl-files";
		vector<list<matt::Student>> hash;
		queue<matt::Student> students;
		ofstream outfile;
		ifstream infile;

		// Read grades
		cout << "Reading in from input.csv\n" << endl;
		infile.open(pathname + "/input.csv");
		if (!infile){
			cout << "File not found" << endl;
			return 1;
		}
		while (infile){
			string str;
			matt::Student stu;
			getline(infile, str);
			if (!str.empty()){
				stringstream ss(str);
				ss >> stu;
				//cout << stu << endl;
				students.push(stu);
			}
		}
		infile.close();

		// Populate hash table
		cout << "\nNow populating the hash table.\n" << endl;
		const int m = students.size();
		hash.resize(m);
		while (!students.empty()){
			if (!students.front().is_null())
				hash[hasher(students.front().uin, m)].push_back(students.front());
			students.pop();
		}

		// Read from roster.csv to get the full class roster
		cout << "\nNow reading in from roster.csv.\n" << endl;
		infile.open(pathname + "/roster.csv");
		if (!infile){
			cout << "File not found" << endl;
			return 1;
		}
		vector<matt::Student> student_vec;
		while (infile){
			string str;
			matt::Student stu;
			getline(infile, str);
			stringstream ss(str);
			ss >> stu;
			//cout << stu << endl;
			if (!stu.is_null())
				student_vec.push_back(stu);
		}
		infile.close();

		// Search hash table 
		// to see if the student did the assignment
		// If the current student in student_vec is found,
		// update the grade
		for (int i = 0; i < student_vec.size(); i++){
			list<matt::Student> current_chain = hash[hasher(student_vec[i].uin, m)];

			// Student is not in hash table
			if (current_chain.size() == 0)
				continue;
			else if (current_chain.size() == 1){
				// Student is found
				if (current_chain.front().uin == student_vec[i].uin){
					student_vec[i].score = current_chain.front().score;
				}
			}
			else if (current_chain.size() > 1){
				// Handle collisions
				list<matt::Student> temp(current_chain);
				while (!temp.empty()){
					if (temp.front().uin == student_vec[i].uin){
						student_vec[i].score = temp.front().score;
					}
					temp.pop_front();
				}
			}
		}

		// Create output.csv
		cout << "\nNow writing data to output.csv\n" << endl;
		outfile.open("output.csv");
		for (auto k : student_vec){
			if (!k.is_null())
				outfile << k << endl;
		}
		outfile.close();

		cout << "\nDone." << endl;

		/*
		// Why not
		if (system(NULL)){
			system("python notice.py");
		}
		*/
		
	}
	catch(invalid_argument& e){
		cerr << "Error: " << e.what() << endl;
	}
	catch(exception& e){
		cerr << "Error: " << e.what() << endl;
	}
	catch(...){
		cerr << "unknown error." << endl;
	}
}
