# CSCE 221 A5
Assignment [here](https://www.dropbox.com/home/CSCE%20221/A5?preview=221-16a-A5.pdf).

Due the night of Tuesday, April 5th.

---

# Description
In this assignment,
given a list of students who had taken a quiz,
we had to find them in a list of all the students and update their quiz grades.
You can find the output csv [here](https://bitbucket.org/!api/2.0/snippets/gaikema/7r5d9/518ef1db07be0cd6727cece0b71810c2f94e1140/files/output.csv).

---

# Instructions

## Linux
Compile with `make`. 
Run with `make run`.
Remove output files using `make clean'.

## Windows
Open the [sln](https://bitbucket.org/gaikema/csce221-a5/src/c4b18414fc593a302baef22a9ad36d4b5010ab35/221A5.sln?at=master&fileviewer=file-view-default) file in Visual Studio.

## Mac
You're on your own.
